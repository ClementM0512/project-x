const {ToRomanus} = require('./romanus.js')

describe('ToRomanus', () => {
    it('Returns I when gives 1', () =>{
        expect(ToRomanus(1)).toEqual('I')
    });
    it('Returns II when gives 2', () =>{
        expect(ToRomanus(2)).toEqual('II')
    });
})